// ->story_state_conversations -> DONE

=== ethan_archetype_introduction
    * { playerArchetype == Jester } [\[Continue\]]
        Hey Ethan, you seem a little down. What's up?  # Actor=Charlie
        -> continue ->
        I don't understand, Charlie. I found out I'm a Jester, but I don't know how to use that to my advantage. I'm afraid I'll never be able to fill the role properly. # Actor=Ethan
        -> continue ->
        Don't worry, Ethan. The Jester archetype has many strengths and weaknesses just like any other. And I'm here to help you understand your potential and realize it.  # Actor=Charlie
        -> continue ->
        (sighs) I just don't know where to start. # Actor=Ethan
        -> continue ->
        Let's start by talking about the strengths of the Jester. They're known for their sense of humor, quick wit, and ability to lighten the mood in any situation.  # Actor=Charlie
        -> continue ->
        (surprised) Really? I never thought about that. # Actor=Ethan
        -> continue ->
        Yes, and the Jester is also known for their fearlessness and ability to take risks without being held back by fear.  # Actor=Charlie
        -> continue ->
        Hmm, I never thought of it that way. # Actor=Ethan
        -> continue ->
        And the Jester also has the ability to bring new perspectives and ideas to the table. They're not afraid to think outside the box and offer a fresh take on things.  # Actor=Charlie
        -> continue ->
        (excited) That sounds like a lot of potential. # Actor=Ethan
        -> continue ->
        Exactly! And don't forget, I'll always be here to guide you and help you understand your potential as a Jester. Together, we can make the most of it.  # Actor=Charlie
        -> continue  ->
    
    * { playerArchetype == Conqueror } [\[Continue\]]
        Hey Ethan, you seem a little down. What's up?  # Actor=Charlie
        -> continue ->
        Charlie, I just found out that my archetype is the conqueror, but I don't think I'm cut out for it. I don't have the strength and courage it requires. # Actor=Ethan
        -> continue ->
        Don't worry Ethan, being a conqueror isn't just about physical strength. It's about having the determination to overcome challenges and never giving up. And I'll always be here to remind you of that.  # Actor=Charlie
        -> continue ->
        But what if I fail? What if I don't live up to my archetype's expectations? # Actor=Ethan
        -> continue ->
        You won't fail, Ethan. You have the potential to be a great conqueror. And even if you stumble along the way, that's okay. The important thing is that you keep pushing forward and never give up on yourself.  # Actor=Charlie
        -> continue ->
        Thank you, Charlie. I really appreciate your support.  # Actor=Ethan
        -> continue ->
        Of course, Ethan. That's what I'm here for. To guide you and help you unlock your full potential. Together, we can conquer anything.   # Actor=Charlie
        -> continue  ->
        
    * { playerArchetype == Orphan } [\[Continue\]]
        Hey Ethan, you seem a little down. What's up?  # Actor=Charlie
        -> continue ->
        Charlie, I just found out that my archetype is the Orphan. I don't understand what it means or what I'm supposed to do with it. # Actor=Ethan
        -> continue ->
        Don't worry Ethan, the Orphan archetype is a complex one, but I'm here to help. This archetype represents a sense of abandonment, insecurity, and independence. But it also holds a great deal of strength and resilience.  # Actor=Charlie
        -> continue ->
        I don't feel very strong or resilient right now. # Actor=Ethan
        -> continue ->
        That's understandable. But the Orphan's journey is to find their own place in the world and make their own way. And I believe that you have the potential to do that.  # Actor=Charlie
        -> continue ->
        How can I find my place in the world and make my own way? # Actor=Ethan
        -> continue ->
        The key is to embrace your independence and not be afraid to take risks. You have a unique perspective and a drive to succeed, and that is a powerful combination. Just trust in yourself and have faith in your abilities, and I know you will succeed.   # Actor=Charlie
        -> continue ->
        Thanks, Charlie. I appreciate your support and encouragement. # Actor=Ethan
        -> continue ->
        Anytime, Ethan. I'm always here to help and guide you on your journey.  # Actor=Charlie
        -> continue  ->
    
    -   ->->
    
    
    // ~ storyState  = sees_light_outside
    
    // -   ->story_state_conversations