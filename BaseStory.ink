EXTERNAL ShowAlert(x)                 
// ShowAlert("message"): Shows an alert message.
EXTERNAL Sequence(x)                  
// Sequence("sequence"): Plays sequencer commands in string x. Note: Use [[x]] instead of {{x}} for shortcuts.
EXTERNAL CurrentQuestState(x)         
// CurrentQuestState("quest"): Returns the current quest state.
EXTERNAL CurrentQuestEntryState(x,y)  
// CurrentQuestEntryState("quest", entry#): Returns the current quest entry state.
EXTERNAL SetQuestState(x,y)           
// SetQuestState("quest", "inactive|active|success|failure"): Sets a quest state.
EXTERNAL SetQuestEntryState(x,y,z)    
// SetQuestEntryState("quest", entry#, "inactive|active|success|failure"): Sets a quest entry state.
EXTERNAL GetBoolVariable(x)           
// Returns the bool value of Dialogue System variable x.
EXTERNAL GetIntVariable(x)            
// Returns the int value of Dialogue System variable x.
EXTERNAL GetStringVariable(x)         
// Returns the string value of Dialogue System variable x.
EXTERNAL SetBoolVariable(x,y)         
// Sets Dialogue System variable x to value of bool y.
EXTERNAL SetIntVariable(x,y)          
// Sets Dialogue System variable x to value of int y.
EXTERNAL SetStringVariable(x,y)       
// Sets Dialogue System variable x to value of string y.


INCLUDE peptalks\jester.ink
INCLUDE peptalks\orphan.ink
INCLUDE peptalks\conqueror.ink
INCLUDE storyparts\starting.ink
INCLUDE archetypeIntro.ink
// INCLUDE storyparts\additional\archetypeintro.ink




LIST playerArchetypes = Conqueror, Jester, Orphan
VAR playerArchetype = Conqueror




LIST exerciseTypes = Physical, Mental, Spiritual, Emotional
VAR  exerciseType = Physical





->story_state_conversations_starting 
->DONE

// -> get_archetype_conversation -> 



    
    


=== get_archetype_conversation ===
    { GetStringVariable("PlayerArchetypeVar"): 
    - "conqueror":  -> conqueror_conversations ->
    - "orphan": ->orphan_conversations->
    - "jester": ->jester_conversations->
    - else: -> DONE
    }
    ->->
    






== continue ==
+ [\[Continue\]]
-
->->

== function limitChoice(localCount) ==
~ return localCount == CHOICE_COUNT()


// == function setPlayerArchetypeVar(_playerArchetype) ==
// ~ temp playerArchetypeIndex = LIST_VALUE(_playerArchetype)
// ~ SetIntVariable("playerArchetypeVar", playerArchetypeIndex)



// == function setCurrentLocationVar(_currentLocation) ==
// ~ temp currentLocationIndex = LIST_VALUE(_currentLocation)
// ~ SetIntVariable("CurrentLocationVar", currentLocationIndex)


=== thread_in_tunnel(-> tunnel_to_run, -> place_to_return_to)
 ~ temp entryTurnChoice = TURNS()
 -> tunnel_to_run ->
 {entryTurnChoice != TURNS():
  -> place_to_return_to      
 }  
 -> DONE

// Fallback functions

== function ShowAlert(x) ==
~ return 1

== function Sequence(x) ==
~ return 1

== function CurrentQuestState(x) ==
~ return "inactive"

== function CurrentQuestEntryState(x,y) ==
~ return "inactive"

== function SetQuestState(x,y) ==
~ return 1

== function SetQuestEntryState(x,y,z) ==
~ return 1

== function GetBoolVariable(x) ==
~ return false

== function GetIntVariable(x) ==
{
    - x == "StoryStateVar": ~ return LIST_VALUE(storyState)
}
~ return 1

== function GetStringVariable(x) ==
{
    - x == "PlayerArchetypeVar":
        {playerArchetype:
            - Conqueror:  ~ return "conqueror"
            - Orphan: ~ return "orphan"
            - Jester: ~ return "jester"
        }
}
~ return ""

== function SetBoolVariable(x,y) ==
~ return 1

== function SetIntVariable(x,y) ==
~ return 1

== function SetStringVariable(x,y) ==
~ return 1