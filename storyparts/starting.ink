LIST storyStatesStarting = main_characters_introduced, sees_light_outside,  knows_about_emerald,  starts_jungle_journey, arrives_river_crossing, reaches_sage_cave, knows_emrald_backstory, knows_next_destination

VAR storyState = main_characters_introduced

~ storyState = storyStatesStarting(GetIntVariable("StoryStateVar"))

LIST currentLocationsStarting = ethan_fireplace, ethan_backyard, jungle_entrypoint, jungle_depth, river_crossing, sage_cave

# This is to initiate the variable
VAR currentLocation = ethan_fireplace

// LIST archeTypes = Conqueror, Orphan, Jester



VAR prevExercise = Physical



=== story_state_conversations_starting
    ~ temp count = CHOICE_COUNT()
    
    * {limitChoice(count) &&  (storyState == sees_light_outside)} [\[Continue\]] 
        ~ currentLocation = ethan_backyard
        ~ SetIntVariable("CurrentLocationVar", LIST_VALUE(currentLocation))
        
        What is that light? Let's go check it out.   # Actor=Charlie
        -> continue ->
        Yeah, let's see. # Actor=Ethan
        -> continue ->
        As they walked towards the backyard, they noticed that the light was coming from under a banyan tree. They approached the tree and saw that something was glowing from underneath its roots.
        -> continue ->
        Look, Ethan! Something is shining under the tree.  # Actor=Charlie
        -> continue ->
        What could it be? Let's dig it up and see. # Actor=Ethan
        -> continue ->
        Ethan and Charlie quickly started digging around the glowing object, and after a few minutes of hard work, they were able to retrieve a small, enchanted book. The cover of the book was made of gold and had the words "The Golden Emerald" inscribed on it.
        -> continue ->
        This must be the enchanted book of the Golden Emerald!  # Actor=Charlie
        -> continue ->
        But why is it here, in our backyard? And why is only the first page written, the rest are blank? # Actor=Ethan
        -> continue ->
        I'm not sure, but I have a feeling that we're meant to find this book and fulfill its purpose.  # Actor=Charlie
        -> continue ->
        You're right, Charlie. We have to find out what the purpose of this book is, and why it was hidden here for us to find. # Actor=Ethan
        -> continue ->
        And with that, Ethan and Charlie began their journey to unravel the mystery of the Golden Emerald and to fulfill its purpose.
        
        ~ storyState = storyState + 1
        ~ SetIntVariable("StoryStateVar", LIST_VALUE(storyState))
        
        
        
    * {limitChoice(count) &&  (storyState == main_characters_introduced)} [\[Welcome\]]
        ~ currentLocation = ethan_fireplace
        ~ SetIntVariable("CurrentLocationVar", LIST_VALUE(currentLocation))
        
        Charlie and Ethan are two inseparable friends, who embark on a journey into the heart of a dangerous jungle.
        -> continue ->
        Ethan is a 15 year old boy, is a fearless adventurer with a thirst for knowledge and a desire to conquer his fears.
        -> continue ->
        while Charlie, who can only communicate with Ethan, is a wise and intuitive spirit animal, 
        -> continue ->
        Despite their differences, they form a powerful bond, as they navigate the trials and tribulations of their journey and help each other grow into the best versions of themselves.
        
        -> ethan_archetype_introduction->
        // ->thread_in_tunnel(-> ethan_archetype_introduction, -> return_to )
        // - (return_to) 
        They were having a deep discussion about the newly-discovered Jungian archetype of Ethan.
        -> continue ->
        Suddenly, they noticed a bright light coming from the backyard.
        
        ~ storyState = storyState + 1
        ~ SetIntVariable("StoryStateVar", LIST_VALUE(storyState))
        
        { GetIntVariable("StoryStateVar") }
        { storyState }
        

    
    

        
    
    * { limitChoice(count) &&  (storyState == knows_about_emerald)} [\[Continue\]]
        Ethan and Charlie were both staring at the book in awe, trying to make sense of what they had just discovered.
        -> continue ->
        What do you think it is, Ethan?  # Actor=Charlie
        -> continue ->
        I have no idea. But there's writing on the first page. Let's see if we can make sense of it. # Actor=Ethan
        -> continue ->
        They both huddled around the page, trying to decipher the script. It was written in an ancient language, but after some time, they were able to translate it.
        -> continue ->
        Look, it's a riddle!
            "To find the path to greatness,
            One must venture to the jungle,
            And seek the sage who lives in peace,
            For he holds the key to your release."  # Actor=Charlie
        -> continue ->
        Ethan and Charlie looked at each other, both realizing the significance of the riddle. They knew that they had to find the sage in the jungle and seek his guidance.
        -> continue ->
        "Ethan, we have to go to the jungle and find this sage. He holds the key to our next mission."  # Actor=Charlie
        -> continue ->
        "But what if the riddle is a trap? What if the sage isn't who he claims to be?" # Actor=Ethan
        -> continue ->
        "Don't worry, Ethan. I'll be with you every step of the way. We'll face any danger together and find the truth behind this riddle."  # Actor=Charlie
        -> continue ->
        And so, Ethan and Charlie set off on their journey to the jungle, determined to find the sage and uncover the secrets of the Golden Emerald.
        ~ storyState = storyState + 1
        ~ SetIntVariable("StoryStateVar", LIST_VALUE(storyState))

        
        <- story_state_quests.physical
        <- story_state_quests.mental
        
    * {(storyState ==  starts_jungle_journey)} [\[Continue\]]
            ~ currentLocation = ethan_fireplace
        ~ SetIntVariable("CurrentLocationVar", LIST_VALUE(currentLocation))
    * {(storyState == arrives_river_crossing)} [\[Continue\]]
    * {(storyState == reaches_sage_cave)} [\[Continue\]]
    * {(storyState == knows_emrald_backstory)} [\[Continue\]]
    * {(storyState == knows_next_destination)} [\[Continue\]]
    
    // - ->story_state_conversations_starting
    - -> DONE
    
    
=== story_state_quests
    = physical
        ~ temp count = CHOICE_COUNT()
        *   { limitChoice(count) &&  (storyState == knows_about_emerald) } [physical quest 1]
            Ethan and Charlie are walking down a narrow jungle path, surrounded by dense trees and foliage.
                -> continue ->
            ETHAN: (excited) This is amazing, Charlie. Look at all these plants and animals!
                -> continue ->
            CHARLIE: (calmly) Yes, it is beautiful. But we must stay focused. We have a mission to complete.
                -> continue ->
            Suddenly, the path ahead of them is blocked by a pile of large boulders.
                -> continue ->
            ETHAN: (frustrated) How are we supposed to get past these boulders?
                -> continue ->
            CHARLIE: (encouraging) Use your physical energy, Ethan. You are strong and agile. You can climb over them.
                -> continue ->
            ETHAN: (determined) You're right, Charlie. I got this. (starts to climb over the boulders)
            # Physical Energy
                -> continue ->
            As Ethan struggles to make his way over the boulders, Charlie encourages him from below.
                -> continue ->
            CHARLIE: (proudly) Well done, Ethan! Your physical energy helped us to overcome this challenge.
                -> continue ->
            Ethan finally reaches the other side of the boulders, panting and sweating.
                -> continue ->
            ETHAN: (excited) That was a workout!
                -> continue ->
            CHARLIE: (smiling) Just a warm-up, Ethan. There are more challenges ahead.
                -> continue ->
            The two continue down the path, ready for whatever the jungle may throw at them next.
            
        * {limitChoice(count) &&  (storyState == knows_about_emerald) } [physical quest 2]
            Ethan and Charlie are walking along a path, surrounded by dense trees.

            ETHAN: (anxious) How are we supposed to get through this dense forest?
            
            Charlie turns to him, his eyes shining with confidence.
            
            CHARLIE: (confident) Use your physical energy, Ethan. You are nimble and quick. You can navigate through the trees.
            
            Ethan takes a deep breath, then nods his head.
            
            ETHAN: (resolved) You're right, Charlie. I'll do my best.
            
            With a determined look, Ethan jumps up and grabs hold of a tree branch. He pulls himself up, then moves from tree to tree, making his way through the dense forest.
            
            CHARLIE: (impressed) Well done, Ethan! Your physical energy helped us to reach the other side of the dense forest.
            
            Ethan lands on the other side of the forest, a proud smile on his face.
            
            ETHAN: (smiling) That was a challenge, but I did it.
            
            CHARLIE: (proud) You did, Ethan. You're growing stronger every day.
            
            Ethan looks at Charlie, then back at the dense forest behind them.
            
            ETHAN: (determined) Let's keep going. There's still more adventure to be had.
            
        * { limitChoice(count) &&  (storyState == knows_about_emerald)  } [physical quest 3] 
            As Ethan and Charlie continue their journey, they come across a fierce animal blocking their path. The animal growls and snarls at them, making Ethan feel afraid and unsure of what to do.
    
            ETHAN: (fearful) What do we do now? That animal looks dangerous.
            CHARLIE: (calmly) Use your physical energy, Ethan. You are brave and courageous. You can defend us against the animal.
            ETHAN: (determined) You're right, Charlie. I can do this. (stands his ground and prepares to confront the animal)
            
            The animal charges at Ethan, but he stays focused and uses his physical energy to defend himself and Charlie. He jumps, dodges, and fights the animal with all his strength, determined to protect his spirit animal.
            
            CHARLIE: (proudly) Well done, Ethan! Your physical energy protected us from danger.
            
            The animal eventually retreats, and Ethan and Charlie continue their journey, proud of their bravery and the strength of their friendship.
        * {limitChoice(count)} Branch 1, third -> DONE
        
        - -> physical
        
        
    = mental  
        ~ temp count = CHOICE_COUNT()
        * {limitChoice(count)} [mental quest 1]
            ETHAN: (frightened) A wild animal just jumped out of the bushes! What do we do?
            CHARLIE: (calmly) Use your physical energy, Ethan. You are brave and courageous. Stand your ground and face it.
            ETHAN: (determined) You're right, Charlie. I can do this. (stands his ground and faces the wild animal)
            
            CHARLIE: (proudly) Well done, Ethan! Your physical energy protected us from danger.
            ETHAN: (panting) That was close! I never knew I had it in me to be so brave.
            CHARLIE: (smiling) You always have it in you, Ethan. You just need to remember to tap into your physical energy when you need it.
            ETHAN: (nodding) Thanks for reminding me, Charlie. I won't forget next time.
            CHARLIE: (encouraging) That's the spirit, Ethan. Remember, your physical energy is a powerful tool that can help you overcome any obstacles you may face.
            ETHAN: (confident) I feel ready for whatever comes next. Let's keep going.
            CHARLIE: (excited) That's the attitude! Let's keep exploring and see what other wonders this jungle has in store for us.
            
        * {limitChoice(count)} Branch 1, second
        * {limitChoice(count)} Branch 1, third
        
        - ->story_state_conversations_starting
        
        = spiritual 
        ~ temp count = CHOICE_COUNT()
        * {limitChoice(count)} [spiritual quest 1]
            ETHAN: (frightened) A wild animal just jumped out of the bushes! What do we do?
            CHARLIE: (calmly) Use your physical energy, Ethan. You are brave and courageous. Stand your ground and face it.
            ETHAN: (determined) You're right, Charlie. I can do this. (stands his ground and faces the wild animal)
            
            CHARLIE: (proudly) Well done, Ethan! Your physical energy protected us from danger.
            ETHAN: (panting) That was close! I never knew I had it in me to be so brave.
            CHARLIE: (smiling) You always have it in you, Ethan. You just need to remember to tap into your physical energy when you need it.
            ETHAN: (nodding) Thanks for reminding me, Charlie. I won't forget next time.
            CHARLIE: (encouraging) That's the spirit, Ethan. Remember, your physical energy is a powerful tool that can help you overcome any obstacles you may face.
            ETHAN: (confident) I feel ready for whatever comes next. Let's keep going.
            CHARLIE: (excited) That's the attitude! Let's keep exploring and see what other wonders this jungle has in store for us.
            
        - ->story_state_conversations_starting
        
        = emotional
        ~ temp count = CHOICE_COUNT()
        * {limitChoice(count)} [emotional quest 1]
            ETHAN: (frightened) A wild animal just jumped out of the bushes! What do we do?
            CHARLIE: (calmly) Use your physical energy, Ethan. You are brave and courageous. Stand your ground and face it.
            ETHAN: (determined) You're right, Charlie. I can do this. (stands his ground and faces the wild animal)
            
            CHARLIE: (proudly) Well done, Ethan! Your physical energy protected us from danger.
            ETHAN: (panting) That was close! I never knew I had it in me to be so brave.
            CHARLIE: (smiling) You always have it in you, Ethan. You just need to remember to tap into your physical energy when you need it.
            ETHAN: (nodding) Thanks for reminding me, Charlie. I won't forget next time.
            CHARLIE: (encouraging) That's the spirit, Ethan. Remember, your physical energy is a powerful tool that can help you overcome any obstacles you may face.
            ETHAN: (confident) I feel ready for whatever comes next. Let's keep going.
            CHARLIE: (excited) That's the attitude! Let's keep exploring and see what other wonders this jungle has in store for us.
            
        - ->story_state_conversations_starting
    
    
    

